use termios::{Termios, TCSANOW, ECHO, ICANON, tcsetattr};
use std::fs::{File, OpenOptions};
use std::io::{BufWriter, BufReader, Read, Write};

macro_rules! clear {
	() => { format!("{c}[2J{c}[1;1H", c = 27 as char).as_bytes() };
}


pub fn start_tui(d: Vec<String> ) -> String {
    let tty = OpenOptions::new()
        .read(true)
        .write(true)
        .open("/dev/tty")
        .expect("error opening new tty");
    
    let stdin = 1;
    let termios = Termios::from_fd(stdin).unwrap();
    let mut new_termios = termios.clone();
    
    new_termios.c_lflag &= !(ICANON | ECHO);
    tcsetattr(stdin, TCSANOW, &mut new_termios).unwrap();

    let mut new_data: Vec<&String> = Vec::new();
    let mut reader = BufReader::new(&tty);
    let mut writer = BufWriter::new(&tty);
    let mut buffer = [0;1];
    let mut input = String::new();
    let mut select = (0, &String::new());
    
    loop {
        new_data = d.iter().filter_map(|x| if x.contains(&input) {Some(x)} else {None}).collect();
        writer.write(format!(">{}\n", input).as_bytes());
        
        if new_data.len() >= 9 {
            let _ = new_data.split_off(8);
            for (i, s) in new_data.iter().enumerate() {
                if i == select.0 {
                    writer.write(format!("\x1b[30;47m{}\x1b[0m\n", s).as_bytes());
                }
                else {
                    writer.write(format!("{}\n", s).as_bytes());
                }
            }
            select.1 = new_data[select.0];
        }
        writer.flush().unwrap();
        reader.read_exact(&mut buffer).unwrap();
        match buffer {
            [10] => break,
            [14] => { if select.0 < new_data.len() -1 { select.0 += 1 } },
            [16] => { if select.0 > 0 { select.0 -= 1} },
            [127] => { input.pop();},
            [c] => {
                input.push(c as char);
            },
        }
        writer.write(clear!());
    }
    tcsetattr(stdin, TCSANOW, & termios).unwrap();
    writer.write(clear!());
    select.1.to_owned()
}

