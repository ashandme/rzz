use std::io::{self, BufRead, Write};
use console::{Term, Key};
mod fzy;

fn main() -> io::Result<()> {
    let stdin = io::stdin();
    let mut reader = stdin.lock();
    let stdout = io::stdout();

    let mut items = Vec::new();
    let mut line = String::new();
    while reader.read_line(&mut line)? > 0 {
        items.push(line.trim_end().to_string());
        line.clear();
    }

    let mut term = Term::stderr();

    let mut query = String::new();
    let mut selected_index = 0;

    loop {
        term.clear_screen()?;
        write!(term, "> {}\r\n", query)?;

        let filtered_items = fzy::fuzzy_filter(&items, &query);

        for (i, item) in filtered_items.iter().enumerate() {
            if i == selected_index {
                write!(term, "> {}\r\n", item)?;
            } else {
                write!(term, "  {}\r\n", item)?;
            }
        }

        term.flush()?;

        match term.read_key()? {
            Key::Char(c) => {
                match c {
                    _ => {
                        query.push(c);
                        selected_index = 0;
                    }
                }
            }
            Key::ArrowUp => {
                if selected_index > 0 {
                    selected_index -= 1;
                }
            }
            Key::ArrowDown => {
                if selected_index < filtered_items.len().saturating_sub(1) {
                    selected_index += 1;
                }
            }
            Key::Backspace => {
                query.pop();
                selected_index = 0;
            }
            Key::Enter => {
                if selected_index < filtered_items.len() {
                    writeln!(stdout.lock(), "{}", filtered_items[selected_index])?;
                    break;
                }
            }
            Key::CtrlC => {
                break;
            }
            _ => {}
        }
    }

    Ok(())
}
