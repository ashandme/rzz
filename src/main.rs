use std::io::{self, BufRead, BufReader, Read};
use std::fs;
mod tui;

fn get_data() -> Vec<String>{
    let stdin = io::stdin();
    // Asi se escribe en ruzt
    let results: Vec<String> = stdin.lock().lines().filter_map(|x| match x { Ok(i) => {if !i.is_empty() { Some(i) } else { None }}, Err(_) => None } ).collect();
    results
}

fn main() {
    let data = get_data();
    let bar = tui::start_tui(data);
    println!("{}", bar);
}
