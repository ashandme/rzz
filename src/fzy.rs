use std::sync::mpsc;
use std::thread;

pub fn fuzzy_filter(items: &[String], query: &str) -> Vec<String> {
    if query.is_empty() {
        return items.to_vec();
    }

    let num_threads = num_cpus::get();
    let chunk_size = (items.len() + num_threads - 1) / num_threads;
    let (tx, rx) = mpsc::channel();

    for chunk in items.chunks(chunk_size) {
        let query = query.to_string();
        let chunk = chunk.to_vec();
        let tx = tx.clone();

        thread::spawn(move || {
            let results: Vec<String> = chunk
                .iter()
                .filter(|item| fuzzy_match(&query, item))
                .cloned()
                .collect();
            tx.send(results).unwrap();
        });
    }

    let mut results = Vec::new();
    for _ in 0..num_threads {
        results.extend(rx.recv().unwrap());
    }

    results.sort_by(|a, b| fuzzy_score(query, b).cmp(&fuzzy_score(query, a)));
    results
}

fn fuzzy_match(query: &str, item: &str) -> bool {
    let mut query_iter = query.chars();
    let mut item_iter = item.chars();

    let mut query_char = query_iter.next();
    let mut item_char = item_iter.next();

    while let (Some(q), Some(i)) = (query_char, item_char) {
        if q.to_lowercase().to_string() == i.to_lowercase().to_string() {
            query_char = query_iter.next();
        }
        if query_char.is_none() {
            return true;
        }
        item_char = item_iter.next();
    }

    query_char.is_none()
}

fn fuzzy_score(query: &str, item: &str) -> i32 {
    let mut score = 0;
    let mut query_iter = query.chars();
    let mut item_iter = item.chars();

    let mut query_char = query_iter.next();
    let mut item_char = item_iter.next();
    let mut consecutive = false;

    while let (Some(q), Some(i)) = (query_char, item_char) {
        if q.to_lowercase().to_string() == i.to_lowercase().to_string() {
            if consecutive {
                score += 2;
            } else {
                score += 1;
            }
            consecutive = true;
            query_char = query_iter.next();
        } else {
            consecutive = false;
        }
        item_char = item_iter.next();
    }
    score
}